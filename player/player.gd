extends CharacterBody2D

@export var speed: float = 300
@export var missile: PackedScene = preload("res://player/missile.tscn")
@export var explosion = preload("res://world/explosion.tscn")
@export var reload_rate: int = 15
var reload = 0
var lives = 2

@export var still = preload("res://player/player.png")
@export var right = preload("res://player/player-right.png")
@export var left= preload("res://player/player-left.png")

var shoot_sound = preload("res://player/player_missile.wav")
var death_sound = preload("res://player/death.wav")

signal death
signal damage

func _physics_process(delta):
	reload -= 1 if reload > 0 else 0
	var direction = Input.get_axis("left", "right")
	if direction < 0:
		velocity = Vector2.LEFT
		$model.set_texture(left)
	elif direction > 0:
		velocity = Vector2.RIGHT
		$model.set_texture(right)
	else:
		velocity = Vector2.ZERO
		$model.set_texture(still)
		
	if Input.is_action_just_pressed("shoot") and reload == 0:
		reload = reload_rate
		var pos = Vector2(position.x, position.y-30)
		get_parent().add_child(missile.instantiate().new(pos))
		%Sound.stream = shoot_sound
		%Sound.play()

	move_and_collide(speed * velocity * delta)

func take_damage(_amount, _pos):
	if lives > 0:
		lives -= 1
		damage.emit()
		explosion.instantiate().new(position, get_parent(), {"sound": death_sound})
	else:
		death.emit(false)
		queue_free()
