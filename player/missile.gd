extends CharacterBody2D

@export var speed: float = 500.0
@export var damage: float = 50.0
var trajectory: Vector2

signal explode(pos)

func _physics_process(delta):
	var collision = move_and_collide(trajectory * delta * speed)
	if collision:
		var collision_pos = collision.get_position()
		collision_pos.y += 1 if trajectory == Vector2.DOWN else -1
		if collision.get_collider().has_method("take_damage"):
			collision.get_collider().take_damage(damage, collision_pos)
			explode.emit(position)
		queue_free()

func new(pos, traj = Vector2.UP, layers = [], masks = [], color = null, scale_ = null):
	trajectory = traj
	position = pos
	add_to_group("bullet")
	for layer in layers:
		set_collision_layer_value(abs(layer), true if layer > 0 else false)
	for mask in masks:
		set_collision_mask_value(abs(mask), true if mask > 0 else false)
	if color != null:
		$Sprite2D.modulate = color
	if scale_ != null:
		$Sprite2D.scale = Vector2(scale_, scale_)
	return self
