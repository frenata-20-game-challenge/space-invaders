extends TileMap

@export var explosion = preload("res://world/explosion.tscn")

func take_damage(_amount, collision_position):
	var damaged_cell = local_to_map(to_local(collision_position))
	explosion.instantiate().new(collision_position, get_parent().get_parent(), {"amount": 3, "size": 4})
	erase_cell(0, damaged_cell)
