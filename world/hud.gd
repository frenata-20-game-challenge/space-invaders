extends Control


func add_points(points_: int):
	%Points/Value.set_text(str(int(%Points/Value.text) + points_))
	
func points() -> int:
	return int(%Points/Value.text)
	
func minus_lives():
	%Lives/Value.set_text(str(int(%Lives/Value.text) - 1))

func lives() -> int:
	return int(%Lives/Value.text)
	
func reset():
	%Lives/Value.set_text("0")
	%Points/Value.set_text("0")
	
