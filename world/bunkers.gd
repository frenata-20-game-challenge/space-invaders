extends Node2D

var bunker_scene = preload("res://world/bunker.tscn")

func _ready():
	for _i in range(5):
		var bunker = bunker_scene.instantiate()
		var x = randi_range(-10, 10)
		bunker.position = Vector2(x*40, 0)
		add_child(bunker)
