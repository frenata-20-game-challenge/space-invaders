extends Node2D

# Sprites and animations hand-crafted with the GIMP
# Audio effects generated with Chiptone: https://sfbgames.itch.io/chiptone

var session = preload("res://world/session.tscn")
var new_score_scene: PackedScene = preload("res://world/new_score.tscn")
var score_menu_scene: PackedScene = preload("res://world/score_menu.tscn")
var score_menu
var high_scores = []
var current_session

func _ready():
	high_scores = load_scores()
	main_menu()

func main_menu():
	if score_menu != null:
		score_menu.queue_free()
	%Menu.visible = true
	%Menu.get_node("VBoxContainer/New").grab_focus()

func new_session():
	current_session = session.instantiate()
	add_child(current_session)
	current_session.session_over.connect(on_session_over)
	%Menu.visible = false

func quit():
	save_scores(high_scores)
	get_tree().quit()
	
func sort(scores_):
	scores_.sort_custom(func(a,b): return a[0] > b[0])
	return scores_
	
func scores(new_score = null):
	score_menu = score_menu_scene.instantiate().new(main_menu)
	add_child(score_menu)
	%Menu.visible = false
	score_menu.get_node("%Back").grab_focus()
	
	var new_high_scores = high_scores.duplicate()
	if new_score != null:
		new_high_scores.append([new_score, null])
	sort(new_high_scores)
	
	for i in range(min(5, len(new_high_scores))):
		score_menu.get_node("Scores").add_child(
			new_score_scene.instantiate()
			.new(new_high_scores[i][1], new_high_scores[i][0], _on_new_high_score))
	
func _on_new_high_score(score, initials):
	high_scores.append([score, initials])
	score_menu.queue_free()
	main_menu()

func save_scores(scores):
	var record = FileAccess.open("user://scores.json", FileAccess.WRITE)
	record.store_line(JSON.stringify(scores))

func load_scores() -> Array:
	var record = FileAccess.open("user://scores.json", FileAccess.READ)
	if record == null:
		return []
	return JSON.parse_string(record.get_line())
	
func on_session_over(points):
	current_session.queue_free()
	if len(high_scores) < 5 or points > sort(high_scores)[-1][0]:
		scores(points)
	else:
		main_menu()
