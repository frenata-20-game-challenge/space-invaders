extends CPUParticles2D

@export var size: int = 15
@export var number: int = 10
@export var sound: AudioStreamWAV = preload("res://enemies/alien_explosion.wav")

func new(pos: Vector2, parent: Node2D, options: Dictionary = {}):
	position = pos
	emitting = true
	emission_sphere_radius = options.get("size", size)
	amount = options.get("number", number)
	%Sound.stream = options.get("sound", sound)
	%Sound.finished.connect(destroy)
	parent.add_child(self)
	return self
	
func destroy():
	queue_free()
