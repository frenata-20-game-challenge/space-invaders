extends Node2D

signal level_over(victory)
signal points_scored(points)
var finished = false

func _on_enemies_arrived(_body):
	if not finished:
		level_over.emit(false)
		finished = true
	
func new(level_over_callable, points_callable):
	level_over.connect(level_over_callable)
	points_scored.connect(points_callable)
	return self
	
func difficulty(config):
	%Enemies.seed_aliens(config.get("fire"), config.get("speed"), config.get("layout"))
	
func points_score(points):
	points_scored.emit(points)
	var enemies = get_tree().get_nodes_in_group("enemies")
	if len(enemies) <= 1:
		level_over.emit(true)
