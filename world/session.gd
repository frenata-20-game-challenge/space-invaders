extends Node2D

signal session_over(points)

var level_scene: PackedScene = preload("res://world/level.tscn")
var player_scene: PackedScene = preload("res://player/player.tscn")
var game_over_sound = preload("res://world/game_over.wav")
var level_start_sound = preload("res://world/victory.wav")
var current_level = 0
var loaded_level
var levels =   {0: {"fire": .0007, "speed": 100, "layout": [[1,0,0,1,0,0,1,0,0,1],
															[0,1,1,0,1,1,0,1,1,0]]},
				1: {"fire": .0008, "speed": 150, "layout": [[1,0,1,0,1,0,1,0,1,0],
															[0,1,0,1,0,1,0,1,0,1],
															[1,0,1,0,1,0,1,0,1,0],
															[0,1,0,1,0,1,0,1,0,1]]},
				2: {"fire": .0009, "speed": 180, "layout": [[1,0,1,1,1,0,1,1,1,0],
															[0,1,0,1,1,1,0,1,1,1],
															[1,1,1,0,1,1,1,0,1,0],
															[0,1,1,1,0,1,0,1,1,1]]},
				3: {"fire": .0010, "speed": 200, "layout": [[1,0,1,0,1,0,1,0,1,0],
															[0,1,0,1,0,1,0,1,0,1],
															[1,0,1,0,1,0,1,0,1,0],
															[0,1,0,1,0,1,0,1,0,1],
															[1,0,1,0,1,0,1,0,1,0],
															[0,1,0,1,0,1,0,1,0,1],
															[1,0,1,0,1,0,1,0,1,0]]},
				4: {"fire": .0011, "speed": 200, "layout": [[1,1,1,1,1,1,1,1,1,1],
															[1,1,1,1,1,1,1,1,1,1],
															[1,1,1,1,1,1,1,1,1,1],
															[1,1,1,1,1,1,1,1,1,1],
															[1,1,1,1,1,1,1,1,1,1],
															[1,1,1,1,1,1,1,1,1,1],
															[1,1,1,1,1,1,1,1,1,1]]},
				5: {"fire": .0012,  "speed": 200, "layout": [[0,1,1,1,1,1,1,1,1,0],
															[1,0,1,1,1,1,1,1,0,1],
															[1,1,0,1,1,1,1,0,1,1],
															[1,1,1,0,1,1,0,1,1,1],
															[1,1,1,1,0,0,1,1,1,1],
															[1,1,1,0,1,1,0,1,1,1],
															[1,1,0,1,1,1,1,0,1,1]]},
}


func _ready():
	new_level(0)
	%Player.death.connect(_on_level_over)
	%Player.damage.connect(_on_lost_life)

func new_level(level):
	if loaded_level != null:
		loaded_level.queue_free()
	%Sound.stream = level_start_sound
	%Sound.play()
	loaded_level = level_scene.instantiate().new(_on_level_over, _on_points)
	loaded_level.difficulty(levels.get(level))
	add_child(loaded_level)
	toast("Level " + str(level))

func _on_level_over(victory):
	if not victory:
		session_over.emit(%HUD.points())
	elif levels.get(current_level + 1) != null:
		current_level += 1
		new_level(current_level)
	else:
		session_over.emit(%HUD.points())

func _on_points(points):
	%HUD.add_points(points)
	
func _on_lost_life():
	%HUD.minus_lives()
	
func toast(message):
	%Message.set_text(message)
	%Message.visible = true
	%Timer.timeout.connect(func(): %Message.visible = false)
	%Timer.start()
