extends HBoxContainer

signal set_initials(score, initials)

func _ready():
	if %Write.visible:
		%Write.grab_focus()
		
func _uppercase(text):
	var pos = %Write.caret_column
	%Write.set_text(%Write.text.to_upper())
	%Write.caret_column = pos
	
	
func new(initials, score, new_initials_handler: Callable):
	set_initials.connect(new_initials_handler)
	
	if initials == null:
		%Read.visible = false
		%Write.visible = true
		%Write.connect("text_submitted", func(new_text): set_initials.emit(score, new_text))
		%Write.connect("text_changed", _uppercase)
	else:
		%Read.set_text(initials)
	%Score.set_text(str(score))
	return self
