extends CharacterBody2D

@export var missile: PackedScene = preload("res://player/missile.tscn")
@export var explosion = preload("res://world/explosion.tscn")
@export var down_shift_rate = 4

var down_shift = 0
var speed
var fire_rate
var multiplier = 1

signal at_edge
signal death(points)

func _ready():
	velocity = Vector2.RIGHT

func _physics_process(delta):
	if down_shift < 0:
		position.y += down_shift_rate
		down_shift += down_shift_rate
	else:
		if move_and_collide(velocity * speed * delta, false, 50, true):
			at_edge.emit()

func shift_direction():
	velocity.x *= -1
	down_shift = -20
	
func _process(_delta):
	if randf_range(0.0, 1.0) < fire_rate * multiplier:
		var shot = missile.instantiate().new(position, Vector2.DOWN, [-1,3], [1, -2], Color(0.33, .93, .54))
		get_parent().add_child(shot)

func take_damage(_amount, _pos):
	death.emit(10)
	explosion.instantiate().new(position, get_parent())
	queue_free()
	
func enrage():
	multiplier = 50
	%Rage.emitting = true
	%RageTimer.start()
	
func calm():
	multiplier = 1
	
func signals(death_handler: Callable, edge_handler: Callable, shift_signal: Signal, enrage_signal: Signal):
	shift_signal.connect(shift_direction)
	enrage_signal.connect(enrage)
	death.connect(death_handler)
	at_edge.connect(edge_handler)


func new(pos, fire_rate_, speed_, color):
	self.position = pos
	self.fire_rate = fire_rate_
	self.speed = speed_
	modulate = color
	return self
