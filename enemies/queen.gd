extends CharacterBody2D

@export var speed: float = 350
@export var fire_rate: float = 0.01
@export var missile: PackedScene = preload("res://player/missile.tscn")
@export var direction = Vector2.LEFT
var explosion = preload("res://world/explosion.tscn")
var explosion_sound = preload("res://enemies/queen_explosion.wav")

signal death(points)

func _physics_process(delta):
	if move_and_collide(direction * speed * delta, false, 10, true):
		queue_free()

func nudge(pos, xdiff):
	pos.x += xdiff
	return pos
	
func _process(_delta):
	if randf_range(0.0, 1.0) < fire_rate:
		get_parent().add_child(missile.instantiate().new(nudge(position, 20), Vector2.DOWN, [-1], [1, -2], Color(0.33, .33, .34), .5))
		get_parent().add_child(missile.instantiate().new(position, Vector2.DOWN, [-1], [1, -2], Color(0.33, .33, .34), .5))
		get_parent().add_child(missile.instantiate().new(nudge(position, -20), Vector2.DOWN, [-1], [1, -2], Color(0.33, .33, .34), .5))		

func take_damage(_amount, _pos):
	death.emit(100)
	explosion.instantiate().new(position, get_parent(), {"sound": explosion_sound, "number": 30, "size": 50})
	queue_free()
	
func new(death_handler: Callable):
	death.connect(death_handler)
	return self
