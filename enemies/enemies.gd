extends Node2D

@export var alien: PackedScene = preload("res://enemies/alien.tscn")
@export var alien_queen: PackedScene = preload("res://enemies/queen.tscn")

@export var rows: int = 4
@export var cols: int = 10
@export var queen_rate: float = 0.005
var queen

signal shift_direction
signal enrage

func alien_death(points):
	if points >= 50:
		enrage.emit()
	get_parent().points_score(points)

func _process(_delta):
	if queen == null and randf_range(0,1) < queen_rate:
		queen = alien_queen.instantiate().new(alien_death)
		if randi() % 2 == 0:
			queen.direction = Vector2.LEFT
			queen.position = Vector2(1000, 30)
		else:
			queen.direction = Vector2.RIGHT
			queen.position = Vector2(0, 30)
		add_child(queen)
			
func seed_aliens(fire_rate, speed, layout):
	var i = 0
	for row in layout:
		var color = Color(randf_range(.1,.9), randf_range(.1,.5), randf_range(.1,.9))
		var j = 0
		for is_enemy in row:
			if is_enemy:
				var enemy = alien.instantiate().new(Vector2(position.x + j * 40, position.y + 40 * i),
													fire_rate, speed, color)
				enemy.signals(alien_death, enemy_at_edge, shift_direction, enrage)
				add_child(enemy)
			j += 1
		i += 1

func enemy_at_edge():
	shift_direction.emit()
